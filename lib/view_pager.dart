import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

class CurrentPageIndex extends InheritedWidget {
  const CurrentPageIndex(
    this.index, {
    Key? key,
    required Widget child,
  }) : super(key: key, child: child);
  final int index;

  static CurrentPageIndex of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<CurrentPageIndex>()
        as CurrentPageIndex;
  }

  @override
  bool updateShouldNotify(CurrentPageIndex old) => index != old.index;
}

class ViewPager extends StatefulWidget {
  const ViewPager({Key? key}) : super(key: key);

  @override
  _ViewPagerState createState() => _ViewPagerState();
}

class _ViewPagerState extends State<ViewPager> {
  final urls = [
    "https://dafftube.org/wp-content/uploads/2014/01/Sample_1280x720_mp4.mp4",
    "https://dafftube.org/wp-content/uploads/2014/01/Sample_1280x720_mp4.mp4",
    "https://dafftube.org/wp-content/uploads/2014/01/Sample_1280x720_mp4.mp4",
    "https://dafftube.org/wp-content/uploads/2014/01/Sample_1280x720_mp4.mp4",
    "https://dafftube.org/wp-content/uploads/2014/01/Sample_1280x720_mp4.mp4"
  ];

  int _currentPageValue = 0;
  late PageController _transController;
  VideoPlayerController? currentVideoController;

  @override
  void initState() {
    _transController = PageController(initialPage: _currentPageValue);
    _transController.addListener(() {
      final currentPage = _transController.page;
      if (currentPage != null && _currentPageValue != currentPage) {
        setState(() {
          _currentPageValue = _transController.page!.toInt();
        });
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: CurrentPageIndex(
        _currentPageValue,
        child: PageView.builder(
          controller: _transController,
          itemCount: urls.length,
          allowImplicitScrolling: true,
          itemBuilder: (context, index) {
            return _VideoViewWidget(
              url: urls[index],
              index: index,
            );
          },
        ),
      ),
    );
  }
}

class _VideoViewWidget extends StatefulWidget {
  final String url;
  final int index;

  const _VideoViewWidget({Key? key, required this.url, required this.index})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _VideoViewState();
}

class _VideoViewState extends State<_VideoViewWidget> {
  late VideoPlayerController _vpc;

  int get _currentIndex => CurrentPageIndex.of(context).index;

  bool get _shouldPause => _currentIndex != widget.index;
  bool _wasPlaying = false;
  Duration _wasPosition = const Duration(milliseconds: 0);
  late VoidCallback vpcListener;
  @override
  void initState() {
    vpcListener = () {
      setState(() {});
    };
    _vpc = VideoPlayerController.network(widget.url)
      ..initialize().then((_) {
        // Ensure the first frame is shown after the video is initialized, even before the play button has been pressed.
        setState(() {});
      })
      ..addListener(vpcListener);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        VideoPlayer(_vpc),
        PlayPauseOverlay(controller: _vpc),
        VideoProgressIndicator(_vpc, allowScrubbing: true),
        Align(
          alignment: Alignment.bottomCenter,
          child: Padding(
            padding: const EdgeInsets.all(16),
            child: Material(
              child: IconButton(
                icon: Icon(!_wasPlaying
                    ? Icons.play_circle_outline_rounded
                    : Icons.pause_circle_rounded),
                onPressed: () {
                  setState(() {
                    if (_wasPlaying) {
                      _wasPlaying = false;
                      _vpc.pause();
                    } else {
                      _wasPlaying = true;
                      _vpc.play();
                    }
                  });
                },
              ),
            ),
          ),
        )
      ],
    );
  }

  @override
  void didChangeDependencies() {
    if (_shouldPause) {
      if (_wasPlaying) {
        _vpc.position.then((value) {
          if (value != null) _wasPosition = value;
          _vpc.pause();
        });
      }
    } else {
      if (_wasPlaying) {
        _vpc.seekTo(_wasPosition).then((value) => _vpc.play());
      }
    }
    print(
        "index:${widget.index}, _currentIndex:$_currentIndex, _wasPlaying:$_wasPlaying, _wasPosition:$_wasPosition");
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _vpc.dispose();
    super.dispose();
  }
}

class PlayPauseOverlay extends StatelessWidget {
  const PlayPauseOverlay({Key? key, this.controller}) : super(key: key);

  final VideoPlayerController? controller;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        AnimatedSwitcher(
          duration: Duration(milliseconds: 50),
          reverseDuration: const Duration(milliseconds: 200),
          child: controller!.value.isPlaying
              ? const SizedBox.shrink()
              : Container(
                  color: Colors.black26,
                  child: const Center(
                    child: Icon(
                      Icons.play_arrow,
                      color: Colors.white,
                      size: 100.0,
                    ),
                  ),
                ),
        ),
        GestureDetector(
          onTap: () {
            controller!.value.isPlaying
                ? controller!.pause()
                : controller!.play();
          },
        ),
      ],
    );
  }
}
